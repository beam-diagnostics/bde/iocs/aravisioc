###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "GT1")
epicsEnvSet("CAMERA_NAME", "Allied Vision Technologies-02-2622A-10012")
epicsEnvSet("GENICAM_DB_FILE", "AVT_GT3300")
epicsEnvSet("ENABLE_CACHING", "1")
epicsEnvSet("XSIZE", "3296")
epicsEnvSet("YSIZE", "2472")
epicsEnvSet("NELEMENTS", "8147712")
###############################################################################
