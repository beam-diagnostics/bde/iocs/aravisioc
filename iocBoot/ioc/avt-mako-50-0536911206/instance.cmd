###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "MAKO2")
epicsEnvSet("CAMERA_NAME", "Allied Vision Technologies-50-0536911206")
epicsEnvSet("GENICAM_DB_FILE", "AVT_Mako_G234B")
epicsEnvSet("ENABLE_CACHING", "1")
epicsEnvSet("XSIZE", "1936")
epicsEnvSet("YSIZE", "1216")
epicsEnvSet("NELEMENTS", "2354176")
###############################################################################
