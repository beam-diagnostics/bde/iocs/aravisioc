###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "BSLR3")
epicsEnvSet("CAMERA_NAME", "Basler-acA2440-20gm-23219608")
epicsEnvSet("GENICAM_DB_FILE", "Basler-acA2440-20gm")
epicsEnvSet("ENABLE_CACHING", "1")
epicsEnvSet("XSIZE", "2448")
epicsEnvSet("YSIZE", "2048")
epicsEnvSet("NELEMENTS", "5013504")
###############################################################################
