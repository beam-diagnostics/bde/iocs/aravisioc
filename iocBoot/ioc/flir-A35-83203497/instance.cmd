###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "FLIR9")
epicsEnvSet("CAMERA_NAME", "FLIR Systems AB-83203497")
epicsEnvSet("GENICAM_DB_FILE", "FLIR_A35")
epicsEnvSet("ENABLE_CACHING", "1")
epicsEnvSet("XSIZE", "320")
epicsEnvSet("YSIZE", "256")
epicsEnvSet("NELEMENTS", "81920")
###############################################################################
