###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "FLIR8")
epicsEnvSet("CAMERA_NAME", "FLIR-Blackfly S BFS-PGE-31S4M-20356218")
epicsEnvSet("GENICAM_DB_FILE", "FLIR_BFS_31S4M")
epicsEnvSet("ENABLE_CACHING", "1")
epicsEnvSet("XSIZE", "2048")
epicsEnvSet("YSIZE", "1536")
epicsEnvSet("NELEMENTS", "3145728")
###############################################################################
