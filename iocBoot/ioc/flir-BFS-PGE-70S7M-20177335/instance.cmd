###############################################################################

#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("LOCATION", "LAB")
epicsEnvSet("DEVICE_NAME", "FLIR6")
epicsEnvSet("CAMERA_NAME", "FLIR-Blackfly S BFS-PGE-70S7M-20177335")
epicsEnvSet("GENICAM_DB_FILE", "FLIR_BFS_70S7M")
epicsEnvSet("ENABLE_CACHING", "1")
epicsEnvSet("XSIZE", "3208")
epicsEnvSet("YSIZE", "2200")
epicsEnvSet("NELEMENTS", "7057600")
###############################################################################
